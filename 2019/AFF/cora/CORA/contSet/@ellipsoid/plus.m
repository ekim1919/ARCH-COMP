function [E] = plus(E1,E2)
% plus - Overloaded '+' operator for the addition of two ellipsoids
%
% Syntax:  
%    [E] = plus(E1,E2)
%
% Inputs:
%    E1 - Ellipsoid object
%    E2 - Ellipsoid object 
%
% Outputs:
%    E - Ellipsoid after addition of two ellipsoids
%
% Example: 
%    E1=ellipsoid([1 0; 0 1]);
%    E2=ellipsoid([1 1; 1 1]);
%    E =E1+E2;
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      30-September-2006 
% Last update:  07-September-2007
%               05-January-2009
%               06-August-2010
%               01-February-2011
%               08-February-2011
%               18-November-2015
% Last revision:---

%------------- BEGIN CODE --------------
%%%%NOTICE: Here, an arbitrary direction l is chosen; probably should be
%%%%done differently, like calculating the sum in multiple directions and
%%%%then intersecting all results which then - in general - is no
%%%%ellipsoid anymore, though. One could approximate this intersection with
%%%%an ellipsoid.
if nargin~=2 || (~isa(E1,'ellipsoid') || ~isa(E2,'ellipsoid'))
    error('Wrong type/number of input arguments');
end
x = randn(length(E.Q),1);
%uniformly sample the n-hypersphere
l = 1/norm(x).*x;
E = lplus(E1,E2,l);
%------------- END OF CODE --------------