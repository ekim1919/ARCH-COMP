function bridgeReach_Krylov()
% updated: 15-November-2016, MA


%start parallel computing
try 
    parpool
catch
    disp('parallel session already running')
end

%system matrix
load bridgeMatrices_withBC
invM = inv(M);
dofs = length(M);
% a = 0.01; %what are good values?
% b = 0.01;
a = 0.1; %what are good values?
b = 0.1;
D = a*K + b*M; %construct D matrix; see mailfrom Daniel Rixen  07 Juni 2015
A = [zeros(dofs) eye(dofs); -invM*K -invM*D];

%obtain dimension
dim = length(A);

%input matrix
load inputIndices
load dof_rem %remaining degrees of freedom
v_str_indices_rem = v_str_indices(dof_rem);
w_LAT_indices_rem = w_LAT_indices(dof_rem);
%version 1: forces are coupled
B_coupled(:,1) = v_str_indices_rem;
B_coupled(:,2) = w_LAT_indices_rem;
%version 2: forces are not coupled
%to be done...
% add zeros
B_coupled = [zeros(dofs,length(B_coupled(1,:))); B_coupled];


%set options --------------------------------------------------------------
options.tStart=0; %start time
%options.tFinal=1e-3; %final time
options.tFinal=1e-4; %final time
%options.tFinal=1e-5; %final time
%options.tFinal=1e-6; %final time
%options.tFinal=3e-7; %final time
options.x0=zeros(dim,1); %initial state for simulation
I = eye(dim);
options.R0=zonotope(sparse([options.x0,0.001*I(:,1:10)])); %initial state for reachability analysis

%options.timeStep=0.04; %time step size for reachable set computation
%options.timeStep=2e-7; %time step size for reachable set computation
options.timeStep=1e-7; %time step size for reachable set computation
options.taylorTerms=4; %number of taylor terms for reachable sets
options.zonotopeOrder=1.5; %zonotope order
options.errorOrder=1;
options.polytopeOrder=2; %polytope order
options.reductionTechnique='girard'; 

%options.KrylovError = 1e-10;
options.KrylovError = eps;
options.KrylovStep = 20;
 

options.plotType='frame';

%options.W = pinv(diag([0.1,0.1,0.5,10,1,1]));
options.originContained = 0;
options.reductionInterval = 1e4;
options.advancedLinErrorComp = 1;
options.tensorOrder = 2;
%--------------------------------------------------------------------------


%obtain uncertain inputs
options.uTrans = [0;0];
options.U = zonotope([0 0.1 0; 0 0 0.1]);

%specify continuous dynamics-----------------------------------------------
linDyn=linearSys('linearDynamics',A,B_coupled); %initialize quadratic dynamics
%--------------------------------------------------------------------------
%[V,H] = Arnoldi_test(linDyn)


%compute reachable set using zonotope bundles
%profile on
tic
iCounter = 1;
[Rcont, compTime] = contReach(linDyn, options, iCounter);
%[Rcont, compTime] = reach(linDyn, options);
toc 
% profile off
% profile viewer

%simulate
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
%generate overall options
opt = odeset(stepsizeOptions);

%initialize
runs=40;
finalTime=options.tFinal;

disp('sim time')
tic
for i=1:runs

    %set initial state, input
    if i<=30
        options.x0=randPointExtreme(options.R0); %initial state for simulation
    else
        options.x0=randPoint(options.R0); %initial state for simulation
    end

    %set input
    if i<=8
        options.u=randPointExtreme(options.U)+options.uTrans; %input for simulation
    else
        options.u=randPoint(options.U)+options.uTrans; %input for simulation
    end
    
    %tic
    %simulate hybrid automaton
    [linDyn,t,x] = simulate(linDyn,options,options.tStart,options.tFinal,options.x0,opt); 
    %compTime.sim = toc
    
    %save results
    cd([coraroot,'/bridgePlots/saves']);
    fileName = ['bridgeReach_Krylov_sim_',num2str(i),'_tf_',num2str(options.tFinal),'_ts_',num2str(options.timeStep),'_',datestr(now,'yyyymmmmdd_HH_MM_SS')];
    fileName = strrep(fileName,'.','_');

    save(fileName, 't', 'x', 'compTime', 'options');
    i
end


%plot
for plotRun=1:4
    if plotRun==1
        projectedDimensions=[2521 2522];
    elseif plotRun==2
        projectedDimensions=[2523 2524];
    elseif plotRun==3
        projectedDimensions=[2525 2526];
    elseif plotRun==4
        projectedDimensions=[2527 2528];
    end

    figure;
    hold on

    
    %plot reachable sets of zonotope
    for i=1:length(Rcont)
        Zproj = project(Rcont{i},projectedDimensions);
        Zproj = reduce(Zproj,'girard',100);
        plotFilled(Zproj,[1 2],[.8 .8 .8],'EdgeColor','none');
    end

    %plot initial set
    plot(options.R0,projectedDimensions,'w-','lineWidth',2);

    %plot simulation results      
    for i=1:length(t)
            plot(x{i}(:,projectedDimensions(1)),x{i}(:,projectedDimensions(2)),'Color',0*[1 1 1]);
    end

end




function [Rcont, compTime] = contReach(sys, options, iCounter)

%load specification
load IHenl

if iCounter == 1
    %obtain factors for initial state and input solution
    for i=1:(options.taylorTerms+1)
        %time step
        r = options.timeStep;
        %compute initial state factor
        options.factor(i)= r^(i)/factorial(i);    
    end

    tic
    %initialize reachable set computations
    [Rnext, options] = initReach_Krylov(sys, options.R0, options);
    compTime.init = toc
else
    %load results
    cd([coraroot,'/bridgePlots/saves']);
    partName = ['bridgeReach_Krylov_iCounter',num2str(iCounter),'_tf_',num2str(options.tFinal),'_ts_',num2str(options.timeStep),'_*'];
    partName = strrep(partName,'.','_');
    file = dir(partName);
    fileName = file.name;

    load(fileName);
    
    % increment iCounter
    iCounter = iCounter + 1;
end

%init t
t = -inf;

while t<options.tFinal
    
    %set iSetStart
    if iCounter == 1
        iSet = 1;
        iSetStart = 1;
    else
        iSet = iSetEnd;
        iSetStart = iSet + 1;
    end
    
    %while final time is not reached
    t=options.tStart + (iSet-1)*options.timeStep;
    
    %delete previous results
    Rcont = [];
    
    tic
    while iSet<iSetStart + 10
    
    
        %save reachable set in cell structure
        Rcont{iSet}=Rnext.ti; 

        %increment time and set counter
        t = t+options.timeStep;
        iSet = iSet+1; 
        options.t=t;
        t

        %compute next reachable set
        [Rnext,options]=post_Krylov(sys,options);
        
        % check enclosure
        if ~(interval(Rnext.ti)<= IHenl)
            disp('specification violated')
            break
        end
    end
    compTime.post = toc
    
    %set variables
    iSetEnd = iSet;
    Rcont{iSet}=Rnext.ti; 
    
    %save results
    cd([coraroot,'/bridgePlots']);
    fileName = ['bridgeReach_Krylov_iCounter',num2str(iCounter),'_tf_',num2str(options.tFinal),'_ts_',num2str(options.timeStep),'_',datestr(now,'yyyymmmmdd_HH_MM_SS')];
    fileName = strrep(fileName,'.','_');

    save(fileName, 'Rcont', 'Rnext', 'iSetStart', 'iSetEnd', 't', 'compTime', 'options', '-v7.3');
    %save(fileName, 'Rcont', 't', 'x', 'compTime', 'options', '-v7.3');
    
    % increment iCounter
    iCounter = iCounter + 1;
end


        
