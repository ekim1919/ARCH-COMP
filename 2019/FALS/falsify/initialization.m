% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)

% Configurations
%%%%%%%%%%%%%%%%
global workers_num logDir maxIter maxEpisodes;
p = gcp();
workers_num = p.NumWorkers;
staliro_dir = 's_taliro/trunk';
[~,git_hash_string] = system('git rev-parse HEAD');
git_hash_string = strrep(git_hash_string,newline,'');
logname = ['data/log', '-', datestr(datetime('now'), 'yyyy-mm-dd-HH-MM'), '-', git_hash_string];
logDir = logname;
maxIter = 1;
maxEpisodes = 1;
algorithms = {'A3C'};
addpath('library', 'benchmarks/transmission', 'benchmarks/powertrain',...
    'benchmarks/SteamCondenser', 'benchmarks/chasing-cars',...
    'benchmarks/neural', 'benchmarks/wind-turbine');

% Initialization
%%%%%%%%%%%%%%%%
if exist('dp_taliro.m', 'file') == 0
    addpath(staliro_dir);
    cwd = pwd;
    cd(staliro_dir);
    setup_staliro;
    cd(cwd);
end

if exist('setup_monitor.m', 'file') == 0
    addpath(fullfile(staliro_dir, 'monitor'));
    cwd = pwd;
    cd(fullfile(staliro_dir, 'monitor'));
    setup_monitor;
    cd(cwd);
end

if ~ 7 == exist(logDir, 'dir')
    mkdir(logDir);
end

if ~ 7 == exist('data/result', 'dir')
    mkdir('data/result');
end
