Theorem "ETCS: Essentials".
Definitions.
  R ep().
  R A().
  R b().
  R m().
End.

ProgramVariables.
  R t.
  R a.
  R z.
  R v.
End.

Problem.
  v>=0&m()-z>=v^2/(2*b())&b()>0&A()>=0&ep()>=0->[{{?m()-z<=v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v);a:=-b();++?m()-z>=v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v);a:=A();}t:=0;{z'=v,v'=a,t'=1&v>=0&t<=ep()}}*]z<=m()
End.


End.

Theorem "ETCS: Proposition 1 (Controllability)".
Definitions.
  R b().
End.

ProgramVariables.
  R m.
  R d.
  R z.
  R v.
End.

Problem.
  (v>=0&d>=0&b()>0)&z<=m->([{z'=v,v'=-b()&v>=0}](z>=m->v<=d)<->v^2-d^2<=2*b()*(m-z))
End.


End.

Theorem "ETCS: Proposition 4 (Reactivity)".
Definitions.
  R ep().
  R A().
  R b().
End.

ProgramVariables.
  R t.
  R sb.
  R a.
  R m.
  R d.
  R em.
  R z.
  R v.
End.

Problem.
  em=0&d>=0&b()>0&ep()>0&A()>0&v>=0->(\forall m \forall z (m-z>=sb&v^2-d^2<=2*b()*(m-z)&v>=0&d>=0&b()>0->[a:=A();t:=0;{z'=v,v'=a,t'=1&v>=0&t<=ep()}](v^2-d^2<=2*b()*(m-z)&v>=0&d>=0&b()>0))<->sb>=(v^2-d^2)/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v))
End.


End.

Theorem "ATC: 2 Aircraft Tangential Roundabout Maneuver".
Definitions.
  R p().
End.

ProgramVariables.
  R x1.
  R c1.
  R omy.
  R om.
  R e1.
  R d1.
  R x2.
  R y1.
  R d2.
  R y2.
  R e2.
  R c2.
End.

Problem.
  (x1-y1)^2+(x2-y2)^2>=p()^2->[{{om:=*;omy:=*;{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-omy*e2,e2'=omy*e1&(x1-y1)^2+(x2-y2)^2>=p()^2}}*c1:=*;c2:=*;om:=*;d1:=-om*(x2-c2);d2:=om*(x1-c1);e1:=-om*(y2-c2);e2:=om*(y1-c1);{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-om*e2,e2'=om*e1&true}}*](x1-y1)^2+(x2-y2)^2>=p()^2
End.


End.

Theorem "ATC: 3 Aircraft Tangential Roundabout Maneuver".
Definitions.
  R p().
End.

ProgramVariables.
  R x1.
  R c1.
  R omy.
  R om.
  R e1.
  R d1.
  R x2.
  R y1.
  R d2.
  R omz.
  R f1.
  R y2.
  R f2.
  R z1.
  R e2.
  R c2.
  R z2.
End.

Problem.
  (x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2->[{{om:=*;omy:=*;omz:=*;{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-omy*e2,e2'=omy*e1,z1'=f1,z2'=f2,f1'=-omz*f2,f2'=omz*f1&(x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2}}*c1:=*;c2:=*;om:=*;d1:=-om*(x2-c2);d2:=om*(x1-c1);e1:=-om*(y2-c2);e2:=om*(y1-c1);f1:=-om*(z2-c2);f2:=om*(z1-c1);{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-om*e2,e2'=om*e1,z1'=f1,z2'=f2,f1'=-om*f2,f2'=om*f1&true}}*]((x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2)
End.


End.

Theorem "ATC: 4 Aircraft Tangential Roundabout Maneuver".
Definitions.
  R p().
End.

ProgramVariables.
  R omu.
  R x1.
  R c1.
  R omy.
  R g2.
  R om.
  R g1.
  R e1.
  R d1.
  R x2.
  R u2.
  R y1.
  R d2.
  R omz.
  R u1.
  R f1.
  R y2.
  R f2.
  R z1.
  R e2.
  R c2.
  R z2.
End.

Problem.
  (x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2&(x1-u1)^2+(x2-u2)^2>=p()^2&(y1-u1)^2+(y2-u2)^2>=p()^2&(z1-u1)^2+(z2-u2)^2>=p()^2->[{{om:=*;omy:=*;omz:=*;omu:=*;{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-omy*e2,e2'=omy*e1,z1'=f1,z2'=f2,f1'=-omz*f2,f2'=omz*f1,u1'=g1,u2'=g2,g1'=-omu*g2,g2'=omu*g1&(x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2&(x1-u1)^2+(x2-u2)^2>=p()^2&(y1-u1)^2+(y2-u2)^2>=p()^2&(z1-u1)^2+(z2-u2)^2>=p()^2}}*c1:=*;c2:=*;om:=*;d1:=-om*(x2-c2);d2:=om*(x1-c1);e1:=-om*(y2-c2);e2:=om*(y1-c1);f1:=-om*(z2-c2);f2:=om*(z1-c1);g1:=-om*(u2-c2);g2:=om*(u1-c1);{x1'=d1,x2'=d2,d1'=-om*d2,d2'=om*d1,y1'=e1,y2'=e2,e1'=-om*e2,e2'=om*e1,z1'=f1,z2'=f2,f1'=-om*f2,f2'=om*f1,u1'=g1,u2'=g2,g1'=-om*g2,g2'=om*g1&true}}*]((x1-y1)^2+(x2-y2)^2>=p()^2&(y1-z1)^2+(y2-z2)^2>=p()^2&(x1-z1)^2+(x2-z2)^2>=p()^2&(x1-u1)^2+(x2-u2)^2>=p()^2&(y1-u1)^2+(y2-u2)^2>=p()^2&(z1-u1)^2+(z2-u2)^2>=p()^2)
End.


End.

Theorem "Robot collision avoidance in two-dimensional space: Static safety".
Definitions.
  R ep().
  R A().
  R b().
  R W().
  R abs(R).
End.

ProgramVariables.
  R yo.
  R t.
  R w.
  R dy.
  R a.
  R r.
  R x.
  R y.
  R dx.
  R xo.
  R v.
End.

Problem.
  (A()>=0&b()>0&ep()>0)&v=0&(x-xo)^2+(y-yo)^2>0&dx^2+dy^2=1->[{{{a:=-b();++?v=0;a:=0;w:=0;++a:=A();w:=*;?-W()<=w&w<=W();r:=*;xo:=*;yo:=*;?r!=0&r*w=v;?abs(x-xo)>v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v)|abs(y-yo)>v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v);}t:=0;}{x'=v*dx,y'=v*dy,v'=a,dx'=-w*dy,dy'=w*dx,w'=a/r,t'=1&t<=ep()&v>=0}}*](x-xo)^2+(y-yo)^2>0
End.


End.

Theorem "Robot collision avoidance in two-dimensional space: Passive safety".
Definitions.
  R V().
  R ep().
  R A().
  R b().
  R W().
  R abs(R).
End.

ProgramVariables.
  R yo.
  R t.
  R w.
  R vyo.
  R dy.
  R vxo.
  R a.
  R r.
  R x.
  R y.
  R dx.
  R xo.
  R v.
End.

Problem.
  (A()>=0&b()>0&ep()>0&V()>=0)&v=0&(x-xo)^2+(y-yo)^2>0&dx^2+dy^2=1->[{{{vxo:=*;vyo:=*;?vxo^2+vyo^2<=V()^2;}{a:=-b();++?v=0;a:=0;w:=0;++a:=A();w:=*;?-W()<=w&w<=W();r:=*;xo:=*;yo:=*;?r!=0&r*w=v;?abs(x-xo)>v^2/(2*b())+V()*v/b()+(A()/b()+1)*(A()/2*ep()^2+ep()*(v+V()))|abs(y-yo)>v^2/(2*b())+V()*v/b()+(A()/b()+1)*(A()/2*ep()^2+ep()*(v+V()));}t:=0;}{x'=v*dx,y'=v*dy,v'=a,dx'=-w*dy,dy'=w*dx,w'=a/r,xo'=vxo,yo'=vyo,t'=1&t<=ep()&v>=0}}*](v>0->(x-xo)^2+(y-yo)^2>0)
End.


End.

Theorem "Chinese Train Control System Level 3 (CTCS-3)".
Definitions.
  R e21().
  R T().
  R e33().
  R e23().
  R e22().
  R CA().
  R e31().
  R x1().
  R e32().
  R min(R,R).
  R Cb().
  R x2().
  R Ca().
End.

ProgramVariables.
  R COswitched.
  R t.
  R lu.
  R v222.
  R conf.
  R v331.
  R v321.
  R v231.
  R a.
  R i.
  R v312.
  R v212.
  R vr1.
  R v221.
  R v322.
  R v211.
  R s.
  R vr2.
  R v.
  R v311.
  R lvl.
  R v332.
  R v232.
End.

Problem.
  CA()=1&Ca()=-0.2&Cb()=-1&T()=0.125&x1()=3200&x2()=6400&e21()=3200&e22()=6400&e23()=9600&e31()=3200&e32()=6400&e33()=9600&a=0&v=0&s=0&i=0&lvl=2&lu=0&COswitched=0&conf=0&v211=105/3.6&v212=100/3.6&v221=105/3.6&v222=100/3.6&v231=45/3.6&v232=40/3.6&v311=255/3.6&v312=250/3.6&v321=255/3.6&v322=250/3.6&v331=0&v332=0->[{{?lvl=2;{?i=2&s>=x1();lvl:=2.5;{?s>x2();lvl:=3;{?COswitched=0&s < e32()&s>e32()-300;conf:=1;COswitched:=1;{v331:=45/3.6;v332:=40/3.6;}{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}++?!(COswitched=0&s < e32()&s>e32()-300);{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}++?s<=x2();{vr1:=(v221+CA()*T())^2;vr1:=min((vr1,v231*v231-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v222+CA()*T())^2;vr2:=min((vr2,v232*v232-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,(v321+CA()*T())^2));vr1:=min((vr1,v331*v331-2*Cb()*e32()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,(v322+CA()*T())^2));vr2:=min((vr2,v332*v332-2*Cb()*e32()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}++?i=0&s>200;i:=1;lu:=1;i:=2;{vr1:=(v211+CA()*T())^2;vr1:=min((vr1,v221*v221-2*Cb()*e21()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,v231*v231-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v212+CA()*T())^2;vr2:=min((vr2,v222*v222-2*Cb()*e21()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,v232*v232-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}++?i=0&s<=200|i=2&s < x1();{vr1:=(v211+CA()*T())^2;vr1:=min((vr1,v221*v221-2*Cb()*e21()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,v231*v231-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v212+CA()*T())^2;vr2:=min((vr2,v222*v222-2*Cb()*e21()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,v232*v232-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}++?lvl=2.5;{?s>x2();lvl:=3;{?COswitched=0&s < e32()&s>e32()-300;conf:=1;COswitched:=1;{v331:=45/3.6;v332:=40/3.6;}{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}++?!(COswitched=0&s < e32()&s>e32()-300);{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}++?s<=x2();{vr1:=(v221+CA()*T())^2;vr1:=min((vr1,v231*v231-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v222+CA()*T())^2;vr2:=min((vr2,v232*v232-2*Cb()*e22()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e23()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,(v321+CA()*T())^2));vr1:=min((vr1,v331*v331-2*Cb()*e32()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,(v322+CA()*T())^2));vr2:=min((vr2,v332*v332-2*Cb()*e32()+2*Cb()*s+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}++?lvl=3;{?COswitched=0&s < e32()&s>e32()-300;conf:=1;COswitched:=1;{v331:=45/3.6;v332:=40/3.6;}{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}++?!(COswitched=0&s < e32()&s>e32()-300);{vr1:=(v331+CA()*T())^2;vr1:=min((vr1,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));vr2:=(v332+CA()*T())^2;vr2:=min((vr2,2*Cb()*s-2*Cb()*e33()+Cb()*CA()*T()*T()+2*Cb()*v*T()));}{?(v+CA()*T())^2>=vr1;a:=Cb();++?(v+CA()*T())^2>=vr2&(v+CA()*T())^2 < vr1;a:=Ca();++?(v+CA()*T())^2 < vr1&(v+CA()*T())^2 < vr2;a:=CA();}}}t:=0;{s'=v,v'=a,t'=1&v>=0&t<=T()}}*]s<=x2()
End.


End.

Theorem "Lunar lander descent guidance (slow descent)".
Definitions.
  R max(R,R).
  R min(R,R).
End.

ProgramVariables.
  R Fc.
  R t.
  R a.
  R m.
  R r.
  R v.
  R isp.
  R alC.
End.

Problem.
  m=1250&r=30&v=-2&Fc=2027.5&a=Fc/m->[{{alC:=max((- 0.01*(a-1.622)-0.6*(v--2)+1.622,1500));Fc:=max((min((Fc/a*alC,5000)),1500));a:=*;?a*m=Fc;}t:=0;{?Fc<=3000;isp:=2500;++?!Fc<=3000;isp:=2800;}{r'=v,v'=a-1.622,a'=a^2/isp,t'=1&r>=0&t<=0.128&a*1100<=Fc&Fc<=a*3000}?t=0.128;}*]((v--2)^2<=0.05^2&v^2<=5^2)
End.


End.