import os
import string
import re
from subprocess import Popen, PIPE
from distutils.dir_util import copy_tree

tools = ['ariadne', 'cora', 'dynibex', 'flowstar', 'isabelle-ode-numerics', 'juliareach']

base_dir = os.getcwd()

# create folders for results
if not os.path.isdir('result'):
    os.mkdir('result')

string_cmd = 'bash ./measure_all'

# merged data for overall results.csv
overall = ""

# iterate over all tools
for t in tools:
    print('['+t+']')
    
    os.chdir(base_dir+'/'+t)

    # call system command via subprocess for pipe redirection
    p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)

    output = str(p.communicate())
    print(output)
    
    if not os.path.isfile('./result/results.csv'):
        print('\tERROR: no results.csv file found in "result" folder')
    else:
        os.chdir(base_dir)
        if not os.path.isdir('result/'+t):
            os.mkdir('result/'+t)
        copy_tree(t+'/result', 'result/'+t)
        with open('result/'+t+'/results.csv') as fp:
            overall += fp.read()
		

with open ('result/results.csv','w') as fp: 
    fp.write(overall) 
    
