function example_hybrid_reach_ARCH20_gearbox_GRBX01()
% example_hybrid_reach_ARCH20_gearbox_GRBX01 -  gearbox benchmark from the
%                                               2020 ARCH competition
%
% Syntax:  
%    example_hybrid_reach_ARCH20_gearbox_GRBX01
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Niklas Kochdumper
% Written:      29-May-2018
% Last update:  13-March-2019
% Last revision:---

%------------- BEGIN CODE --------------


% Parameter ---------------------------------------------------------------

% initial set
int = interval([0;0;-0.0168;0.0029;0],[0;0;-0.0166;0.0031;0]);
params.R0 = zonotope(int);

% initial location, final location, and final time
params.startLoc = 1;
params.finalLoc = 2;
params.tFinal = 0.2;


% Reachability Options ----------------------------------------------------

% settings for continuous reachability
options.timeStepLoc{1} = 1.1e-3;

options.zonotopeOrder = 20;
options.taylorTerms = 3;

% settings for computing guard intersections
options.guardIntersect = 'zonoGirard';
options.enclose = {'box','pca'};


% Hybrid Automaton --------------------------------------------------------

HA = gearbox();



% Reachability Analysis ---------------------------------------------------

tic
HA = reach(HA,params,options);
tComp = toc;



% Verification ------------------------------------------------------------

R = get(HA,'reachableSet');

tic

% constraint x5 < 20
res = 1;

for i = 1:length(R)
   for j = 1:length(R{i}.OT)
      temp = interval(project(R{i}.OT{j},5));
      if supremum(temp) >= 20
         res = 0;
         break;
      end
   end
end

tVer = toc;

disp(['specifications verified: ',num2str(res)]);
disp(['computation time: ',num2str(tVer + tComp)]);



% Visualization -----------------------------------------------------------

figure
hold on

% plot reachable set
plotReach(HA,[3,4],'b');

% plot tooth of gear
locs = get(HA,'location');
trans = get(locs{1},'transition');
guard1 = get(trans{1},'guard');
guard2 = get(trans{2},'guard');

hs1 = halfspace(guard1.h.c(3:4),guard1.h.d);
hs2 = halfspace(guard2.h.c(3:4),guard2.h.d);

axis([-2e-2,-3e-3,-1e-2,1e-2]);
box on

plot(hs1,[1,2],'r');
plot(hs2,[1,2],'r');


%------------- END OF CODE --------------