classdef linearSysDT < contDynamics
% linearSysDT class (linSysDT: discrete-time linear system)
%
% Syntax:  
%    object constructor: Obj = linearSysDT(varargin)
%                        Obj = linearSysDT(name,A,B)
%                        Obj = linearSysDT(name,A,B,c)
%                        Obj = linearSysDT(name,A,B,c,C)
%                        Obj = linearSysDT(name,A,B,c,C,D)
%                        Obj = linearSysDT(name,A,B,c,C,D,k)
%    copy constructor: Obj = otherObj
%
% Description:
%    Generates a discrete-time linear system object according to the 
%    following first-order difference equations:
%       x(i+1) = A x(i) + B u(i) + c
%       y(i) = C x(i) + D u(i) + k
%
% Inputs:
%    name - name of system
%    A - state matrix
%    B - input matrix
%    c - constant input
%    C - output matrix
%    D - throughput matrix
%    k - output offset
%
% Outputs:
%    Obj - Generated Object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      20-Mar-2020 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    A = []; % system matrix
    B = 1; % input matrix
    c = []; % constant input
    C = 1; % output matrix
    D = []; % throughput matrix
    k = []; % output offset
end

methods
    %class constructor
    function obj = linearSysDT(varargin)
        % default IDs for states, inputs, and outputs
        stateIDs = ones(length(varargin{2}),1);
        inputIDs = 1; %default
        outputIDs = 1; %default
        if nargin>=3
            % default IDs for inputs
            B = varargin{3};
            inputIDs = ones(length(B(1,:)),1);
            if nargin >= 5
                C = varargin{5};
                % nr of outputs
                outputIDs = ones(length(C(:,1)),1);
            end
        end
        % instantiate parent class
        obj@contDynamics(varargin{1},stateIDs,inputIDs,outputIDs); 
        % 3 or more inputs
        if nargin>=3
            obj.A = varargin{2};
            obj.B = varargin{3};
            if nargin >= 4
                obj.c = varargin{4};
            end
            if nargin >= 5
                obj.C = varargin{5};
            end
            if nargin >= 6
                obj.D = varargin{6};
            end
            if nargin == 7
                obj.k = varargin{7}; 
            end
        end
    end
end
end

%------------- END OF CODE --------------