function res = isIntersecting(obj1,obj2,varargin)
% isIntersecting - determines if zonotope obj1 intersects obj2
%
% Syntax:  
%    res = isIntersecting(obj1,obj2)
%    res = isIntersecting(obj1,obj2,type)
%
% Inputs:
%    obj1 - zonotope object
%    obj2 - conSet object
%    type - type of check ('exact' or 'approx')
%
% Outputs:
%    res - 1/0 if set is intersecting, or not
%
% Example: 
%    zono1 = zonotope([0 1 1 0;0 1 0 1]);
%    zono2 = zonotope([2 -1 1 0;2 1 0 1]);
%    zono3 = zonotope([3.5 -1 1 0;3 1 0 1]);
%
%    isIntersecting(zono1,zono2)
%    isIntersecting(zono1,zono3)
%
%    figure
%    hold on
%    plot(zono1,[1,2],'b');
%    plot(zono2,[1,2],'g');
%
%    figure
%    hold on
%    plot(zono1,[1,2],'b');
%    plot(zono3,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: conZonotope/isIntersecting

% Author:       Niklas Kochdumper
% Written:      21-Nov-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % get zonotope object
    if ~isa(obj1,'zonotope')
        temp = obj1;
        obj1 = obj2;
        obj2 = temp;
    end
    
    % call function for other set representations
    if isa(obj2,'halfspace') || isa(obj2,'conHyperplane') || ...
       isa(obj2,'mptPolytope')
   
        res = isIntersecting(obj2,obj1,varargin{:});
   
    else
        
        res = isIntersecting(conZonotope(obj1),obj2,varargin{:});
        
    end     
end

%------------- END OF CODE --------------