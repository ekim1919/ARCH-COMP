function options = checkOptionsReachInner(obj,options)
% checkOptionsReachInner - checks if all necessary options are there and 
%                          have valid values
%
% Syntax:  
%    options = checkOptionsReachInner(obj,options)
%
% Inputs:
%    obj     - nonlinearSys object (unused)
%    options - options for nonlinearSys (1x1 struct)
%
% Outputs:
%    options - options for nonlinearSys (1x1 struct)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: reachInner

% Author:       Niklas Kochdumper
% Written:      14-Aug-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    checkName = 'checkOptionsReachInner';

    % check start and final time
    options = check_tStart_tFinal(obj, options, checkName);

    % check time step
    check_timeStep(options, obj, checkName);

    % check algorithm
    options = check_algInner(obj,options);

    if strcmp(options.algInner,'proj')

        % check algorithm settings
        options = check_R0int(obj,options);
        options = check_taylorOrder(obj,options);
        options = check_taylmOrder(obj,options);
        
        % check inputs
        if isfield(options,'U') || isfield(options,'u')
           error('Uncertain inputs are not supported for algorithms "proj"!'); 
        end

        % valid fields
        validFields = {'tStart';'tFinal';'R0';'timeStep';'taylorOrder';...
                        'taylmOrder';'algInner'};

        % warnings for unused options (overdefined)
        warning(printRedundancies(options,validFields));
        
    elseif strcmp(options.algInner,'parallelo')
        
        options_ = rmfield(options,'algInner');
        checkOptionsReach(obj,options_);

    elseif strcmp(options.algInner,'scale')

        % check algorithm settings
        options = check_R0int(obj,options);
        options = check_timeStepInner(obj,options);
        options = check_contractor(obj,options);
        options = check_iter(obj,options);
        options = check_splits(obj,options);
        options = check_factor(obj,options);
        options = check_orderInner(obj,options);

        % valid fields
        validFields = getValidFields(class(obj));

        validFields(ismember(validFields,'tensorOrder')) = [];
        validFields(ismember(validFields,'maxError')) = [];
        validFields(ismember(validFields,'reductionInterval')) = [];
        validFields(ismember(validFields,'alg')) = [];

        validFields = [validFields;{'algInner';'N';'contractor'; ...
                                    'iter';'splits';'orderInner'; ...
                                    'factor';'timeStepInner'}];

        % warnings for unused options (overdefined)
        warning(printRedundancies(options,validFields));

        % default options
        options.alg = 'poly';
        options.tensorOrder = 3;
        options.R0 = polyZonotope(options.R0);
    end
end


% Auxiliary Functions -----------------------------------------------------

function options = check_algInner(obj,options)
% check if a valid algorithm is selected

    validAlg = {'scale','proj','parallelo'};
    option = 'algInner';
    strct = 'options';

    if ~isfield(options,option)
        error(printOptionMissing(obj,option,strct));
    elseif ~any(strcmp(validAlg,options.algInner))
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_R0int(obj,options)
% check if initial set is an interval and has the correct dimension

    option = 'R0';
    strct = 'options';

    if ~isfield(options,option)
        error(printOptionMissing(obj,option,strct));
    elseif size(center(options.R0),1) ~= obj.dim
        error(printOptionSpecificError(obj,option,...
            'R0 and object must have the same dimension.'));
    elseif ~isa(options.R0,'interval')
        if (isa(options.R0,'zonotope') && isInterval(options.R0)) || ...
           (isa(options.R0,'polyZonotope') && isInterval(options.R0))
            options.R0 = interval(options.R0);
        else
            error('Only intervals are supported for R0');
        end
    end  
end

function options = check_taylorOrder(obj,options)
% check if the taylor order is valid
    
    option = 'taylorOrder';
    strct = 'options';

    if ~isfield(options,option)
        error(printOptionMissing(obj,option,strct));
    elseif options.taylorOrder <= 0 || mod(options.taylorOrder,1.0) ~= 0
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_taylmOrder(obj,options)
% check if the taylor model order is valid
    
    option = 'taylmOrder';
    strct = 'options';

    if ~isfield(options,option)
        error(printOptionMissing(obj,option,strct));
    elseif options.taylmOrder <= 0 || mod(options.taylmOrder,1.0) ~= 0
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_timeStepInner(obj,options)
% check if the time step for inner-approximation is valid
    
    option = 'timeStepInner';
    strct = 'options';
    N = length(options.tStart:options.timeStep:options.tFinal)-1;

    if ~isfield(options,option)
        options.N = N;
    else
        range = options.tStart:options.timeStepInner:options.tFinal;
        if mod(options.timeStepInner,options.timeStep) > eps || ...
           abs(range(end)-options.tFinal) > eps
            error(printOptionOutOfRange(obj,option,strct)); 
        else
            options.N = round(options.timeStepInner/options.timeStep,0);
        end
    end
end

function options = check_contractor(obj,options)
% check if the specified contractor is valid
    
    option = 'contractor';
    strct = 'options';
    def = 'linearize';
    validAlg = {'linearize','forwardBackward','polyBox'};

    if ~isfield(options,option)
        options.contractor = def;
    elseif ~any(strcmp(validAlg,options.contractor))
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_iter(obj,options)
% check if the number of iterations is valid
    
    option = 'iter';
    strct = 'options';
    def = 2;

    if ~isfield(options,option)
        options.iter = def;
    elseif options.iter <= 0 || mod(options.iter,1.0) ~= 0
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_splits(obj,options)
% check if the number of splits is valid
    
    option = 'splits';
    strct = 'options';
    def = 8;

    if ~isfield(options,option)
        options.splits = def;
    elseif options.splits <= 0 || mod(options.splits,1.0) ~= 0
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_factor(obj,options)
% check if the scaling factor is valid
    
    option = 'factor';
    strct = 'options';
    def = 'auto';

    if ~isfield(options,option)
        options.factor = def;
    elseif ischar(options.factor) && ~strcmp(options.factor,'auto')
        error(printOptionOutOfRange(obj,option,strct));
    elseif options.factor <= 0 || options.factor > 1
        error(printOptionOutOfRange(obj,option,strct));
    end
end

function options = check_orderInner(obj,options)
% check if the zonotope order for the inner-approximation is valid
    
    option = 'orderInner';
    strct = 'options';
    def = 5;

    if ~isfield(options,option)
        options.orderInner = def;
    elseif options.orderInner < 1
        error(printOptionOutOfRange(obj,option,strct));
    end
end

%------------- END OF CODE --------------