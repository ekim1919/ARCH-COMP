function res = isemptyobject(S)
% isemptyobject - checks whether a contSet object contains any information
%    at all; consequently, the set is equivalent to the empty set 
%
% Syntax:  
%    res = isemptyobject(S)
%
% Inputs:
%    S - contSet object
%
% Outputs:
%    res - true/false
%
% Example: 
%    E = ellipsoid([1,0;0,1]);
%    isemptyobject(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isempty

% Author:       Mark Wetzlinger
% Written:      03-June-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% init result
res = true;

% read class of contSet object
classname = class(S);

% instantiate fully-empty object of same class
S_empty = eval([classname '();']);

% read properties
props = properties(S);
nrProps = length(props);

% properties must be same as in fully-empty object of same class
for p=1:nrProps

    % property of contSet object
    prop = S.(props{p});

    % corresponding property of fully-empty set
    prop_empty = S_empty.(props{p});

    % type of property
    type = class(prop);

    % check depends on type
    switch type

        case 'logical'
            if prop ~= prop_empty
                res = false; return
            end

        case {'double','cell','sym'}
            if ~isequal(prop,prop_empty)
                res = false; return
            end

        case {'char','string'}
            if ~strcmp(prop,prop_empty)
                res = false; return
            end

        case 'Polyhedron'
            % caution: this check will be changed once the mptPolytope
            % class is substituted
            if ~isempty(S)
                res = false; return
            end

        otherwise
            % check if the property is a contSet object (e.g., halfspace
            % object in conHyperplane object)
            if isa(prop,'contSet')
                % recursive call
                if ~isemptyobject(prop)
                    res = false; return
                end
            else
                throw(CORAerror('CORA:specialError','Unknown data type'));
            end
    end
    
end

%------------- END OF CODE --------------